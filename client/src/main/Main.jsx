import React, {useState, useEffect } from 'react';
import 'react-dropdown/style.css';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
//STYLES
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import Dropdown from 'react-dropdown';
import './main.css'
//FUNCTIONS
import axios from 'axios';


const Main = () => {
    const [from, setFrom] = useState(`BTC`);
    const [to, setTo] = useState(`RUB`);
    const [give, setGive] = useState(1);
    const [get, setGet] = useState(0);
    const [from_arr, setFromArr] = useState([]);
    const [to_arr, setToArr] = useState([]);
    const [courseto, setCoursTO] = useState(0);

    const GetFromTo = async () => {
        console.log(123)
        let res = await axios.get('http://localhost:1337/api/get_from_to')
        console.log(res.data.from)
        setFromArr(res.data.from)
        setToArr(res.data.to)
    }

    const GetCourse = (from_in,to_in) => {
        axios.post('http://localhost:1337/api/get_course',
        {
          from: from_in,
          to: to_in,
        }) .then(({ data }) => {
            console.log(data)
            setCoursTO(data);
            let resGet = data * give
            setGet(resGet)
          })
    }
    useEffect(async () => {
        GetFromTo()
        GetCourse('BTC','RUB');
    }, [])
   
    const onDropdownSelectedfrom = async (event) => {
         setFrom(event.value)
         GetCourse(event.value,to)
         
    }
   
    const onDropdownSelectedto = (event) => {
        setTo(event.value)
        GetCourse(from,event.value)

    }

    const onChangegive_final = ({ target }) => {
        setGive(target.value);
        target.value = target.value.replace(',','.')
        let res_get = Number(target.value) * courseto
        if (isNaN(res_get)){
            setGet('VVEDI CHISLO')
        }else{
        setGet(res_get.toFixed(7)*1)}
    }

    const onChangeget_final = ({ target }) => {
        setGet(target.value);
        let res_give = Number(target.value) / courseto
        if (isNaN(res_give)){
            setGive('VVEDI CHISLO')
        }else{
        setGive(res_give.toFixed(7)*1)}

    }

    return (
        <div className='block'>
            <div className="course">
            Now course<br/>
            1 {from}  = {courseto*1} {to}
            </div>
            <br/>
            <Grid
                container
            >
                <Grid
                    item
                    md={6}
                    xs={6}
                >
                    <Dropdown
                    className="block_drop"
                        options={from_arr}
                        onChange={onDropdownSelectedfrom}
                        value={from}
                    />
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={6}
                >
                    <Input
                        type='text'
                        onChange={onChangegive_final}
                        value={give}
                    ></Input>
                </Grid>
            </Grid>

            <br />
            <Grid
                container
            >
                  <Grid
                    item
                    md={6}
                    xs={6}
                >
                <Dropdown
                className="block_drop"
                    options={to_arr}
                    onChange={onDropdownSelectedto}
                    value={to}
                />
                </Grid>
                <Grid
                    item
                    md={6}
                    xs={6}
                >
                <Input
                    type='text'
                    onChange={onChangeget_final}
                    value={get}
                    className="input"></Input>
                    </Grid>
                    <Grid
                    item
                    md={12}
                    xs={12}
                >
                    Курсы получаются с помощью => https://api.binance.com/api/v3/ticker/price?symbol= <br/>
                    Значения которые получаются находятся в файле => data.js<br/>
                    Значение всегда по принципу from = крипта || to = фиат <br/>
                    В калькуляторе работает декоратор - можно вводить нецелые числа как через " , " так и через " . "<br/>
                    Калькулятор работает в обе стороны можжно вводить как колличество крипты так и колличество фиата <br/>
                    Значение будет округлено до 7 знака <br/>
                    При введение строки в поле в другом поле будет ввыедено предупреждение <br/>
                    Так как мы работаем через http api есть задержка примерно 0.5 секунды <br/>
                </Grid>
            </Grid>
            <div className='name'>
            designed by Balashov1337<br/>
            https://gitlab.com/Balashov1337/
            </div>
        </div>


    )


}
export default Main;