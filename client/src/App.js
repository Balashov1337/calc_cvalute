
import Main from './main/Main';

function App() {
  return (
    <div className="App">
       <div className="content" id="outer-container">
          <div id="page-wrap">
            <Main />
          </div>
        </div>
    </div>
  );
}

export default App;
