if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

const path = require('path');
const bodyParser = require('body-parser');
const axios = require('axios');

//SERVER
const express = require('express');
const app = express();
const cors = require('cors');
//DATA
const data = require("./data")

app.use(
    express.static(__dirname + '/client/build', { dotfiles: 'allow' }),
    express.static(__dirname + '/client/public'),
    bodyParser.json({ limit: '100mb', extended: false }),
    bodyParser.urlencoded({ limit: '100mb', extended: false }),
    express.urlencoded({ extended: false }),
    cors(),
);


app.get('/api/get_from_to',(req, res, done) => {
    // console.log({ from: data.arr_from, to: data.arr_to})
    res.json({ from: data.arr_from, to: data.arr_to})
});

app.post('/api/get_course', async (req, res, done) => {
//    console.log(req.body['from'], req.body['to'])
   let result =  await get_binance_data(req.body['from'], req.body['to'])
   res.json(result)
});

app.get('/', (req, res) => {
    res.sendFile(path.resolve('client', 'build', 'index.html'))
})

app.get('*', (req, res) => {
    res.sendFile(path.resolve('client', 'build', 'index.html'))
})


app.listen(process.env.PORT, () => {
    console.log(`Example app listening at http://localhost:${process.env.PORT}`)
})


get_binance_data = async (from,to) =>{
    const standart_url = 'https://api.binance.com/api/v3/ticker/price?symbol='
    let req_url = standart_url+from+to
    let data = await axios.get(req_url)
    return(data.data.price)
}